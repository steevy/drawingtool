var canvas
var context
var lineStartX, lineStartY
var lines = []
var offsetX, offsetY
var tempLine

class Line  {
    constructor(startX, startY, endX, endY) {
      this.startX = startX
      this.startY = startY
      this.endX = endX
      this.endY = endY
    }

    draw(ctx) {
        ctx.beginPath()
        ctx.moveTo(this.startX, this.startY)
        ctx.lineTo(this.endX, this.endY)
        ctx.stroke()
    }
  }

function init() {
    canvas = document.getElementById("drawing-area")
    context = canvas.getContext("2d")

    redraw()

    var rect = canvas.getBoundingClientRect();
    offsetX = rect.x
    offsetY = rect.y

    canvas.addEventListener('click', clickReporter, false)
    canvas.addEventListener('mousemove', moveReporter, false)
}

function redraw() {
    clearContext()
    drawBox()
    drawStoredLines()
    drawTempline()
}

function drawTempline() {
    if (tempLine) {
        tempLine.draw(context)
    }
}

function clickReporter(e) {
    var currentX = e.clientX - offsetX
    var currentY = e.clientY - offsetY

    if (tempLine){
        addLine(tempLine.startX, tempLine.startY, tempLine.endX, tempLine.endY)
        tempLine = null
        redraw()
    } else {
        tempLine = new Line(currentX, currentY, currentX, currentY)
    }
}

function moveReporter(e) {
    var currentX = e.clientX - offsetX
    var currentY = e.clientY - offsetY

    if (tempLine) {
        tempLine.endX = currentX
        tempLine.endY = currentY

        redraw()
    }    
}

function addLine(startX, startY, endX, endY) {
    lines.push(new Line(startX, startY, endX, endY))
}

function clearContext() {
    context.clearRect(0, 0, canvas.width, canvas.height);
}

function drawStoredLines() {
    if(lines.length == 0){ return }
    
    // redraw each stored line
    for(var i = 0; i < lines.length; i++) {
        lines[i].draw(context)
    }
}

function drawBox() {
    context.fillStyle = "black"
    context.strokeRect(20, 20, canvas.width-20, canvas.height-20)
}
